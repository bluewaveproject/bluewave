# bluewave
Blue Wave is an Arch-based Linux distribusion, with the sole focus of making Linux simple and easy to use. It provides three different versions made for different user preferences.

NOTES:

This repository basically has the entire source code for all the currently available Blue Wave versions. If you want to build the ISO yourself, then CD into the folder (the folder that was created when you git cloned it) and then do a mkarchiso -v (whatever version you want). But note, you need to have archiso installed to do this. 
Go to https://gitlab.com/bluewaveproject/bluewave/-/wikis to read the wiki/guide. It has the info you need before installing.

contains some code from arch linux gui so thanks to them. their github is this: https://github.com/arch-linux-gui

The source code featured here may not 100% perfect. Due to the transfer process from GitHub to GitLab, it imported the old February 2023 release. (Why? I don't know.)

Made in The Philippines 🇵🇭

![oceandsktop](https://gitlab.com/bluewaveproject/bluewave/-/raw/main/images/ocean_new.png)
![riverdsktop](https://gitlab.com/bluewaveproject/bluewave/-/raw/main/images/river_new.png)
![depthsdsktop](https://gitlab.com/bluewaveproject/bluewave/-/raw/main/images/depths_1.png)


Download link to ISOs:

Cu1rrent: https://tinyurl.com/202305bluewave1

Legacy (outdated, not maintained): https://bit.ly/3yfBNf8
