import gi
import subprocess
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

def on_button1_clicked(widget):
    print("Button 1 clicked.")
    subprocess.Popen(["/bin/konsole", "-e", "doas", "pacman", "-Syu" ])

def on_button2_clicked(widget):
    print("Button 2 clicked.")
    subprocess.Popen(("/bin/konsole", "-e", "doas", "pacman", "-Scc"))

def on_button3_clicked(widget):
    print("Button 3 clicked.")
    subprocess.Popen(("/bin/systemsettings"))

def on_button4_clicked(widget):
    print("Button 4 clicked.]")
    subprocess.Popen(("/bin/konsole", "-e", "rm", "-rf", "/$HOME/.config/autostart/blwv.desktop", "&&", "exit"))

win = Gtk.Window()
win.connect("destroy", Gtk.main_quit)

label = Gtk.Label()
label.set_text("Welcome to Blue Wave!")
label.set_size_request(200, 30)

label2 = Gtk.Label()
label2.set_text("An Arch-based distro that's made for everyone.")
label2.set_size_request(200, 30)

button1 = Gtk.Button(label="Update")
button1.connect("clicked", on_button1_clicked)

button2 = Gtk.Button(label="Clean system")
button2.connect("clicked", on_button2_clicked)

button3 = Gtk.Button(label="Open system settings")
button3.connect("clicked", on_button3_clicked)

button4 = Gtk.Button (label="Remove from autostart and exit")
button4.connect("clicked", on_button4_clicked)

box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
box.pack_start(label, False, False, 0)
box.pack_start(label2, False, False, 0)
box.pack_start(button1, False, False, 0)
box.pack_start(button2, False, False, 0)
box.pack_start(button3, False, False, 0)
box.pack_start(button4, False, False, 0)

win.add(box)
win.show_all()

Gtk.main()
